$(document).ready(function () {
    var placeHeader = function(){
        if ($(window).scrollTop() > 263) {
            $(".site-header.fixed").addClass('active');
        } else {
            $(".site-header.fixed").removeClass('active');
        }
    };

    $(window).on("scroll", function () {
        placeHeader();
    });

    placeHeader();

    $(".search-item img").on("click", function (e) {
        $(this).parent().find('input').toggleClass('active');

        if ($(this).attr('src').indexOf('search') !== -1) {
            $(this).attr('src', '/img/close.svg');
        } else {
            $(this).attr('src', '/img/search.svg');
        }

        e.preventDefault();
        return false;
    });

    $('html').on('click', function(e) {
        if (!$(e.target).hasClass('search-icon') && !$(e.target).hasClass('search-input')) {
            $('.search-form').hide();
        }
    });

    $('#newsletter').on('submit', function (e) {
        e.preventDefault();
        let form = $(this);

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            dataType: 'json',
            success (response) {
                if (response['status'] === 'ok') {
                    $("#newsletter").html(response['html']);
                }
            }
        });

        return false;
    });

    $('.search-form-mobile').on('click', '.search-icon', function () {
        if ($(this).attr('src').indexOf('search') !== -1) {
            $(this).attr('src', '/img/close.svg');
        } else {
            $(this).attr('src', '/img/search.svg');
        }
        $('.search-form-mobile').toggleClass('active');
    });

    $('header').on('click', '.menu', function () {
        $(this).toggleClass('active');
        $(".site-header").eq(0).toggleClass('active');
    });
});
