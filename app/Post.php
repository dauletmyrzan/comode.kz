<?php

namespace App;

use Encore\Admin\Facades\Admin;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;
use Jenssegers\Date\Date;

class Post extends Model implements Viewable
{
    use InteractsWithViews;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tag');
    }

    public function tile()
    {
        return $this->hasOne(Tile::class);
    }

    public function getCreatedAttribute(): string
    {
        return Date::createFromTimestamp(strtotime($this->created_at))->format('j F');
    }

    public function getCategoryNameAttribute()
    {
        return $this->category() && $this->category()->count() > 0 ? $this->category()->first()->name : '';
    }

    public function getColorAttribute()
    {
        return $this->category() && $this->category()->count() > 0 ? $this->category()->first()->color : '';
    }

    public function getYoutubeAttribute()
    {
        $regex = "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#";
        preg_match($regex, $this->link, $matches);

        return $matches[0] ?? '';
    }

    public function user()
    {
        return $this->belongsTo(\Encore\Admin\Auth\Database\Administrator::class, 'user_id', 'id');
    }

    public function getAuthorAttribute()
    {
        return $this->user->name ?? 'Неизвестен';
    }

    public function getViewsAttribute()
    {
        return views($this)->count() + $this->view_count;
    }

    public function getBackgroundImageAttribute()
    {
        return $this->category->img ?? 'images/1520х289 (10).jpg';
    }

    public function getBreadcrumbsAttribute()
    {
        return '<div class="breadcrumb">
            <a href="/">Главная</a>&nbsp;/&nbsp;
            <a href="/category/' . $this->category->code . '">' . $this->categoryName . '</a>&nbsp;/&nbsp;
            <a href="javascript:void(0);">' . $this->title . '</a>
        </div>';
    }

    public function getSimilarAttribute()
    {
        return Post::query()->whereHas('category', function ($q) {
            return $q->where('code', $this->category->code);
        })
            ->where('id', '!=', $this->id)
            ->take(3)
            ->get();
    }

    public function getImgAttribute($img)
    {
        if (file_exists(storage_path('app/public') . '/media/post/' . $img) ||
            file_exists(storage_path('app/public') . '/media/images/' . $img) ||
            file_exists(storage_path('app/public') . '/images/' . $img) ||
            file_exists(storage_path('app/public') . '/assets1/' . $img)) {
            return $img;
        }

        $exploded = explode(".", $img);
        $ext      = strtolower(end($exploded));
        array_pop($exploded);

        if (count($exploded) > 1) {
            $img = implode(".", $exploded);
        } else {
            $img = $exploded[0];
        }

        return $img . "." . $ext ?? '../img/post/default-post.png';
    }

    public function getIsKzAttribute()
    {
        $letters = [
            'ә',
            'і',
            'ң',
            'ү',
            'ұ',
            'қ',
            'ө',
            'һ',
        ];
        $case    = mb_str_split($this->title);

        foreach ($case as $letter) {
            if (in_array($letter, $letters, true)) {
                return true;
            }
        }

        return false;
    }

    public function getTitleHtmlAttribute()
    {
        if (mb_strlen($this->title) >= 50) {
            $this->title = mb_substr($this->title, 0, 50) . ' ...';
        }

        return $this->title;
    }
}
