<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', function () {
        return redirect('/admin/posts');
    })->name('home');

    $router->get('/comode-tv', 'PostController@comodeTv');
    $router->post('/comode-tv', 'PostController@store');
    $router->get('/comode-tv/create', 'PostController@createComodeTv');
    $router->get('/comode-tv/{id}', 'PostController@show');
    $router->get('/comode-tv/{id}/edit', 'PostController@edit');

    $router->get('/columnists', 'PostController@columnists');
    $router->post('/columnists', 'PostController@store');
    $router->get('/columnists/create', 'PostController@createColumnists');
    $router->get('/columnists/{id}', 'PostController@show');
    $router->get('/columnists/{id}/edit', 'PostController@edit');

    $router->resource('/posts', PostController::class);

    $router->resource('/categories', CategoryController::class);

    $router->resource('/hashtags', TagController::class);

    $router->resource('/footer', FooterController::class);

    $router->resource('/tiles', TileController::class);

    $router->resource('/gallery', GalleryController::class);
});
