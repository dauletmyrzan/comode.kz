<?php

namespace App\Admin\Controllers;

use App\GalleryImage;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use App\Gallery;

class GalleryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Галерея';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Gallery);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('title', 'Название');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Gallery::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('title', 'Название');
        $show->images('Картинки')->as(function ($images) {
            $html = '';

            foreach ($images as $img) {
                $html .= "<img src='/media/gallery/" . $img->image . "' width='100'>";
            }

            return $html;
        })->unescape();

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Gallery);

        $form->display('id', __('ID'));
        $form->text('title', 'Название');
        $form->hasMany('images', 'Картинки', function (Form\NestedForm $form) {
            $form->text('title', 'Подпись');
            $form->image('image', 'Картинка')->required();
        });

        return $form;
    }
}
