<?php

namespace App\Admin\Controllers;

use App\Post;
use App\Tile;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TileController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Плитки на главной';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Tile);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', 'Название');
        $grid->column('img', 'Картинка фона')->image('', '150');
        $grid->column('post.title', 'Текущая статья')->link(function($post) {
            return '/admin/posts/' . $post->id;
        });

        $grid->disableCreateButton();

        $grid->actions(function ($actions) {
            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Tile::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('created_at', 'Дата создания');
        $show->field('updated_at', 'Дата обновления');
        $show->field('name', 'Название');
        $show->field('img', 'Фоновое изображение')->image();
        $show->field('post.title', 'Статья');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Tile);

        $form->display('id', __('ID'));
        $form->text('name', 'Название');

        $form->image('img', 'Фоновое изображение')->help('Если не загрузить картинку, будет использована картинка из статьи.');
        $form->select('post_id', 'Статья')->options(Post::all()->pluck('title', 'id'));
        $form->display('created_at', 'Дата создания');
        $form->display('updated_at', 'Дата обновления');

        $form->saving(function (Form $form) {
            if (!$form->img) {
                $form->model()->img = Post::query()->where('id', $form->post_id)->pluck('img')->first();
            }
        });

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }
}
