<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Category;
use Illuminate\Support\Str;

class CategoryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Категории';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Category);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', 'Название');
        $grid->column('color', 'Цвет фона');
        $grid->column('img', 'Изображение фона');
        $grid->column('order', 'Порядок в меню')->sortable();

        $grid->filter(function ($filter) {
            $filter->like('name', 'Заголовок');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Category::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', 'Название');
        $show->field('color', 'Цвет фона');
        $show->field('img', 'Изображение фона');
        $show->field('created_at', 'Дата создания');
        $show->field('updated_at', 'Дата обновления');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Category);

        $form->display('id', __('ID'));
        $form->text('name', 'Название');
        $form->color('color', 'Цвет фона');
        $form->image('img', 'Изображение фона');
        $form->number('order', 'Порядок в меню');
        $form->hidden('code');
        $form->saving(function (Form $form) {
            $form->code = Str::slug($form->name);
        });
        $form->switch('show_in_menu', 'Показывать в верхнем меню')->states([
            'on' => ['value' => 1, 'text' => 'Да'],
            'off' => ['value' => 0, 'text' => 'Нет'],
        ]);

        return $form;
    }
}
