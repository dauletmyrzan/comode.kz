<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Support\Str;

class PostController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Статьи';

    /**
     * Make a grid builder.
     *
     * @param  array|null $params
     * @return Grid
     */
    protected function grid(array $params = null)
    {
        $grid = new Grid(new Post);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('title', __('Заголовок'));
        $grid->column('user.name', 'Автор');
        $grid->column('created_at', __('Создано'))->display(function ($column) {
            return date('d.m.Y H:i', strtotime($column));
        });

        $grid->filter(function (Grid\Filter $filter) {
            $filter->like('title', 'Заголовок');
            $filter->in('category_id', 'Категория')->select(Category::all()->pluck('name', 'id'));
        });

        if (!$params) {
            $grid->filter(function ($filter) {
                $filter->in('is_slider', 'Слайдер')->checkbox([
                    '1' => 'Показать только главный слайдер'
                ]);
                $filter->in('is_tv', 'Comode TV')->checkbox([
                    '1' => 'Только Comode TV'
                ]);
            });
        }

        if (isset($params['is_tv'])) {
            $grid->model()->where('is_tv', $params['is_tv']);
            $grid->setTitle('Comode TV');
        }

        if (isset($params['is_slider'])) {
            $grid->model()->where('is_slider', $params['is_slider']);
            $grid->setTitle('Главный слайдер');
        }

        if (isset($params['is_columnist'])) {
            $grid->model()->where('is_columnist', $params['is_columnist']);
            $grid->setTitle('Колумнисты');
        }

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $post = Post::findOrFail($id);
        $show = new Show($post);

        $show->field('id', __('ID'));
        $show->field('title', 'Заголовок');
        $show->field('body', 'Контент');
        $show->field('audio', 'Аудио')->audio();
        $show->field('img');
        $show->field('is_tv', 'Comode TV');
        $show->field('user.name', 'Автор');
        $show->field('views', 'Просмотры');
        $show->field('created_at', 'Создано');
        $show->field('updated_at', 'Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @param  array|null $params
     * @return Form
     */
    protected function form(array $params = null)
    {
        $form = new Form(new Post);

        $form->display('id', __('ID'));
        $form->hidden('slug', 'Slug');
        $form->hidden('user_id', 'Author')->value(Admin::user()->id);
        $form->text('title', 'Заголовок');
        $form->file('audio', 'Аудио')
            ->rules('mimes:application/octet-stream,audio/mpeg,mpga,mp3,wav');
        $form->ckeditor('body', 'Контент');

        if (!$params) {
            $form->switch('is_slider', 'Показывать на главном слайдере')->states([
                'on' => [
                    'value' => 1,
                    'text' => 'Да',
                ],
                'off' => [
                    'value' => 0,
                    'text' => 'Нет',
                ]
            ]);
            $form->switch('is_tv', 'Comode TV')->states([
                'on' => [
                    'value' => 1,
                    'text' => 'Да',
                ],
                'off' => [
                    'value' => 0,
                    'text' => 'Нет',
                ]
            ]);
            $form->switch('is_columnist', 'Колумнисты')->states([
                'on' => [
                    'value' => 1,
                    'text' => 'Да',
                ],
                'off' => [
                    'value' => 0,
                    'text' => 'Нет',
                ]
            ]);
        } else if (isset($params['is_tv'])) {
            $form->hidden('is_tv')->value('1');
        } else if (isset($params['is_slider'])) {
            $form->hidden('is_slider')->value('1');
        } else if (isset($params['is_columnist'])) {
            $form->hidden('is_columnist')->value('1');
        }

        $form->url('link', 'Ссылка на YouTube');

        $form->select('category_id', 'Категория')->options(Category::all()->pluck('name', 'id'));
        $form->multipleSelect('tags', 'Хештеги')->options(Tag::all()->pluck('name', 'id'));
        $form->image('img', 'Картинка');
        $form->image('slider_image', 'Картинка для слайдера (десктоп)');

        $form->saving(function (Form $form) {
            $form->slug = Str::slug($form->title);
        });

        return $form;
    }

    public function comodeTv(Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['index'] ?? trans('admin.list'))
            ->body($this->grid(['is_tv' => 1]));
    }

    public function createComodeTv(Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['create'] ?? trans('admin.create'))
            ->body($this->form(['is_tv' => 1]));
    }

    public function columnists(Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['index'] ?? trans('admin.list'))
            ->body($this->grid(['is_columnist' => 1]));
    }

    public function createColumnists(Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['create'] ?? trans('admin.create'))
            ->body($this->form(['is_columnist' => 1]));
    }
}
