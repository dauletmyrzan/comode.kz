<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Footer;

class FooterController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Футер (подвал) сайта';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Footer);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', 'Название');
        $grid->column('url', 'Ссылка');
        $grid->column('column', 'Номер колонки');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Footer::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', 'Название');
        $show->field('url', 'Ссылка');
        $show->field('column', 'Номер колонки');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Footer);

        $form->display('id', __('ID'));
        $form->text('name', 'Название');
        $form->text('url', 'Ссылка');
        $form->select('column', 'Номер колонки')->options([
            1 => 1,
            2 => 2,
            3 => 3
        ]);

        return $form;
    }
}
