<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function getBreadcrumbsAttribute()
    {
        return '<div class="breadcrumb">
            <a href="/">Главная</a>&nbsp;/&nbsp;
            <a href="/category/' . $this->code . '">' . $this->name . '</a>&nbsp;/&nbsp;
        </div>';
    }
}
