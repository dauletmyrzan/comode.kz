<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tile extends Model
{
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
