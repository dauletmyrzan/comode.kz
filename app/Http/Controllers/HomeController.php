<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $slider     = Post::query()->where('is_slider', 1)->orderBy('created_at', 'desc')->take(5)->get();
        $tiles      = Tile::all();
        $newPosts   = Post::query()
            ->where('is_slider', 0)
            ->where('is_tv', 0)
            ->where('is_columnist', 0)
            ->where('approved', 1)
            ->doesntHave('tile')
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        $comodeTv   = Post::query()
            ->where('is_tv', 1)
            ->where('approved', 1)
            ->take(5)
            ->get();
        $columnists = Post::query()
            ->where('is_columnist', 1)
            ->where('approved', 1)
            ->take(6)
            ->get();

        return view('welcome', compact([
            'slider',
            'tiles',
            'newPosts',
            'comodeTv',
            'columnists'
        ]));
    }

    public function newsletter(Request $request)
    {
        $request->validate([
            'name'  => 'required|regex:/^[A-Za-zА-Яа-я]+$/u',
            'email' => 'required|email',
        ]);

        $name  = $request->get('name');
        $email = $request->get('email');

        if (DB::table('newsletter')->where('email', $email)->count() === 0) {
            DB::table('newsletter')->insert([
                'name'  => $name,
                'email' => $email,
            ]);

            $html = 'Вы успешно подписались на нашу рассылку!';
        } else {
            $html = 'Вы уже подписаны на нашу рассылку :)';
        }

        return response()->json([
            'status' => 'ok',
            'html'   => sprintf("<h5 class='my-2'>%s</h5>", $html),
        ]);
    }
}
