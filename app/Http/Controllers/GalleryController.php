<?php
namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function get(Request $request)
    {
        $ids       = (array) $request->get('ids');
        $galleries = Gallery::query()->whereIn('id', $ids)->get();

        if (!$galleries) {
            return response()->json([
                'status'  => 'error',
                'message' => 'Слайдеры не загрузились.',
            ]);
        }

        foreach ($galleries as $gallery) {
            foreach ($gallery->images as $key => $image) {
                if (!file_exists(storage_path('app/public') . '/media/gallery/' . $image->image)) {
                    unset($gallery->images[$key]);
                }
            }
        }

        return response()->json([
            'status' => 'ok',
            'data'   => view('ajax.galleries', compact('galleries'))->render(),
        ]);
    }
}
