<?php
namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class PostController
{
    public function show(Request $request)
    {
        $post      = Post::query()->where('slug', $request->slug)->first();
        $expiresAt = now()->addMinutes(30);

        views($post)->cooldown($expiresAt)->record();

        return view('posts.show', compact(['post']));
    }

    public function getColumnists(Request $request)
    {
        $columnists = Post::query()
            ->where('is_columnist', 1)
            ->orderBy('id', 'asc')
            ->offset($request->get('count'))
            ->limit(2)
            ->get();

        return response()->json([
            'status' => 'ok',
            'html'   => view('ajax.columnists', ['columnists' => $columnists])->render()
        ]);
    }

    public function index(Request $request)
    {
        $code  = preg_replace('/[^a-z0-9-]/', '', $request->route('code'));
        $posts = collect();

        if ($code === 'semija-i-deti') {
            $ids = [
                4,
                79,
                10,
                14,
            ];
            $categoryName = 'Семья и дети';
        } elseif($code === 'krasota-i-zdorove') {
            $ids = [
                52,
                53
            ];
            $categoryName = 'Красота и здоровье';
        } elseif($code === 'nastojashhie-istorii') {
            $ids = [
                107
            ];
            $categoryName = 'Настоящие истории';
        } elseif($code === 'moda-i-stil') {
            $ids = [
                39,
                54,
                49,
                36,
            ];

            $categoryName = 'Мода и стиль';
        } elseif($code === 'zvezdy') {
            $ids = [
                48,
                19,
                47,
                44,
                76,
            ];
            $categoryName = 'Звезды';
        } elseif($code === 'muzcina') {
            $ids = [
                3,
                18,
                65,
                56,
                60,
                37,
                81,
            ];
            $categoryName = 'Мужчина';
        }

        if (isset($ids)) {
            $categories = Category::query()->whereIn('id', $ids)->get();

            foreach ($categories as $category) {
                $posts = $posts->merge($category->posts()->where('approved', 1)->get());
            }

            $posts    = $posts->paginate(config('app.pagination.per_page'));
            $category = $categories->first();
            $category->name = $categoryName;
        } else {
            $category = Category::query()->where('code', $code)->first();
            $posts    = $category->posts()->where('approved', 1)->get()->paginate(config('app.pagination.per_page'));
        }

        if (!$category) {
            abort(404);
        }

        return view('posts.index', compact(['category', 'posts']));
    }


    public function search(Request $request)
    {
        $q = preg_replace("/[^A-Za-zА-Яа-я0-9 ?!,]/u", '', $request->get('query'));

        $posts = Post::query()
            ->where('approved', 1)
            ->where(function ($query) use ($q) {
                $query->where('title', 'like', '%' . $q . '%');
                $query->orWhere('body', 'like', '%' . $q . '%');
            })
            ->paginate(10);

        return view('search', compact('posts'));
    }
}
