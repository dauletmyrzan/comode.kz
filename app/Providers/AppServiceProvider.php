<?php

namespace App\Providers;

use App\Category;
use App\Footer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app('url')->forceRootUrl(env('APP_URL'));
        /**
         * Paginate a standard Laravel Collection.
         *
         * @param int $perPage
         * @param int $total
         * @param int $page
         * @param string $pageName
         * @return array
         */
        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(env('FORCE_HTTPS',false)) {
            url()->forceScheme('https');
        }

        View::composer('layout.app', function ($view) {
            $menu      = Category::query()->where('show_in_menu', 1)->orderBy('order')->get();

            foreach ($menu as $item) {
                if ($item->code === 'deti') {
                    $item->code = 'semija-i-deti';
                    $item->name = 'Семья и дети';
                }

                if ($item->code === 'krasota') {
                    $item->code = 'krasota-i-zdorove';
                    $item->name = 'Красота и здоровье';
                }

                if ($item->code === 'protiv-nasiliya') {
                    $item->code = 'nastojashhie-istorii';
                    $item->name = 'Настоящие истории';
                }

                if ($item->code === 'muzcina-i-zenshhina') {
                    $item->code = 'muzcina';
                    $item->name = 'Мужчина';
                }
            }

            $footerObj = Footer::all();
            $footer    = collect();
            $arr       = [];

            foreach ($footerObj as $item) {
                $footerItem       = collect();
                $footerItem->name = $item->name;
                $footerItem->url  = $item->url;

                $arr[$item->column][] = $footerItem;
            }

            foreach ($arr as $key => $arrItem) {
                $footer->put($key, $arrItem);
            }

            $view->with('menu', $menu)->with('footer', $footer);
        });
    }
}
