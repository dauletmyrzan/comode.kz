<?php

return [
    'regex' => 'Поле ":attribute" имеет неверный формат.',
    'required' => 'Заполните ":attribute"!',

    'attributes' => [
        'name' => 'Имя',
        'email' => 'Email'
    ],
];
