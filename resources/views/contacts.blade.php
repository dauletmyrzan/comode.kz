@extends('layout.app')

@section('content')
    <div class="container main-content">
        <h1>Связаться с нами</h1>
        <div class="row">
            <div class="col-md-5">
                <div class="contacts-row">
                    <p class="contacts-label mb-0">По вопросам рекламы и предложений о сотрудничестве обращайтесь по email:</p>
                    <a href="mailto:info@comode.kz">info@comode.kz</a>
                </div>

                <div class="contacts-row mt-3">
                    <h2 class="contacts-title">Адрес офиса:</h2>

                    <address class="contacts-address">Алматы, Керемет микрорайон, 3Б<br>
                    </address>

                    <address class="contacts-address">Тел: +7 701 729 4834 (Татьяна Пастухова)</address>

                    <address class="contacts-address">+7 701 744 0453 (Меруерт Есмуратова)</address>
                </div>
            </div>
            <div class="col-md-7">
                <div class="border">
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Af8017430472860e2c80e2896feb634368e34cd79d6f9fedba702aba62870df6b&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
            </div>
        </div>
        <hr>
    </div>
@endsection
