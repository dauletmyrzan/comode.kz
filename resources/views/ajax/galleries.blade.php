@foreach($galleries as $gallery)
    <div class="slick-slider my-5 px-5" data-id="{{ $gallery->id }}">
        @foreach($gallery->images as $image)
            <div>
                <img src="{{ '/media/gallery/' . $image->image }}" alt="Картинка" width="100%">
            </div>
        @endforeach
    </div>
@endforeach
