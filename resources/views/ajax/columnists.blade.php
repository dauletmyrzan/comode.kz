@foreach($columnists as $item)
    <div class="col-md-6 mb-3">
        <div class="columnist-post">
            <a href="/post/{{ $item->slug }}"><img src="/storage/{{ $item->img }}" alt="Картинка"></a>
            <div class="columnist-post-details">
                <a href="/post/{{ $item->slug }}"><div class="columnist-post-title">{{ $item->title }}</div></a>
                <div class="columnist-post-author">{{ $item->author }}</div>
                <div class="columnist-post-date">{{ $item->created }} <span class="views-count">{{ $item->views }}</span></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <button class="show-more show-more-columnists">Смотреть больше</button>
    </div>
@endforeach
