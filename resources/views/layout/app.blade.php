<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, viewport-fit=cover">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Женский портал Comode.kz - лучший информационно-развлекательный онлайн журнал и сайт для женщин">
    <title>Женский портал Comode.kz - лучший информационно-развлекательный онлайн журнал и сайт для женщин</title>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W5VJ353');</script>
    <!-- End Google Tag Manager -->

    <link rel="icon" type="image/svg+xml" href="/img/favicon.svg">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/app.css?v=' . env('STATIC_VERSION')) }}">
    <link rel="stylesheet" href="{{ asset('/css/media.css?v=' . env('STATIC_VERSION')) }}">
    @yield('styles')
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5VJ353" height="0" width="0"
                style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="container-fluid px-0">
        <header class="blog-header">
            <div class="main-wrapper">
                <div class="blog-header-content">
                    <form action="/search" class="search-form-mobile">
                        <input type="text" name="query" required class="search-input">
                        <img src="{{ asset('/img/search.svg') }}" alt="Картинка" class="search-icon" width="20">
                    </form>

                    <a class="blog-header-logo text-dark" href="/"><img src="{{ asset('/img/logo.png') }}"></a>
                </div>
                <div class="menu"></div>
            </div>

            <nav class="site-header py-3 {{ url()->current() === \URL::to('/') ? '' : 'border-bottom' }}">
                <div class="container d-flex flex-column flex-md-row justify-content-between">
                    @foreach($menu as $item)
                        @if($item->code === 'krasota')
                            <a class="py-2 d-none d-md-inline-block {{ \Request::is('category/krasota-i-zdorove') ? 'active' : '' }}" href="/category/krasota-i-zdorove">Красота и здоровье</a>
                        @elseif($item->code === 'zdorove')
                        @else
                            <a class="py-2 d-none d-md-inline-block {{ \Request::is('category/' . $item->code) ? 'active' : '' }}" href="/category/{{ $item->code }}">{{ $item->name }}</a>
                        @endif
                    @endforeach
                    <a class="py-2 d-none d-md-inline-block search-item" href="javascript:void(0)">
                        <form action="/search" class="search-form">
                            <input type="text" name="query" required class="search-input">
                        </form>
                        <img src="/img/search.svg" alt="Картинка" class="search-icon" width="20">
                    </a>
                </div>
                <div class="socials-mobile">
                    <a href="https://t.me/comode_kz"><div class="social-item social-telegram"></div></a>
                    <a href="https://www.facebook.com/comodekz/"><div class="social-item social-facebook"></div></a>
                    <a href="https://www.instagram.com/comodekz/"><div class="social-item social-instagram"></div></a>
                    <a href="https://www.youtube.com/channel/UCrxsr_AUUm1X-UYsR73EnJw"><div class="social-item social-youtube"></div></a>
                </div>
            </nav>

            <nav class="site-header fixed border-bottom hidden-mobile pt-1 pb-2 bg-white">
                <div class="container d-flex flex-column flex-md-row justify-content-between">
                    @foreach($menu as $item)
                        @if($item->code === 'krasota')
                            <a class="py-2 d-none d-md-inline-block" href="/category/krasota-i-zdorove">Красота и здоровье</a>
                        @elseif($item->code === 'zdorove')
                        @else
                            <a class="py-2 d-none d-md-inline-block" href="/category/{{ $item->code }}">{{ $item->name }}</a>
                        @endif
                    @endforeach
                    <a class="py-2 d-none d-md-inline-block search-item" href="#"><img src="/img/search.svg" alt="Картинка" width="20"></a>
                </div>
            </nav>
        </header>
        @yield('content')
        <div class="container-fluid stylized-container footer mb-5">
            <div class="mb-5">
                <img src="{{ asset('/img/logo.png') }}" class="footer-logo" alt="Картинка" width="300">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        @foreach($footer as $key => $column)
                            <div class="col-md-4 @if($key > 1) hidden-mobile @endif">
                                <ul class="footer-ul">
                                    @foreach($column as $item)
                                        <li class="footer-item"><a href="{{ $item->url }}">{{ $item->name }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6 text-md-right">
                    <div class="socials">
                        <a href="https://t.me/comode_kz"><div class="social-item social-telegram"></div></a>
                        <a href="https://www.facebook.com/comodekz/"><div class="social-item social-facebook"></div></a>
                        <a href="https://www.instagram.com/comodekz/"><div class="social-item social-instagram"></div></a>
                        <a href="https://www.youtube.com/channel/UCrxsr_AUUm1X-UYsR73EnJw"><div class="social-item social-youtube"></div></a>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <span>&copy; <?=date('Y')?>, Comode.kz.</span><span>Женский информационно-развлекательный портал.</span>
        </footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="{{ asset('/js/app.js?v=' . env('STATIC_VERSION')) }}"></script>
    @yield('scripts')
</body>
</html>
