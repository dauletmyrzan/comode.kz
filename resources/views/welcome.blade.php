@extends('layout.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/swiper-bundle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/modal-video.min.css') }}">
@endsection

@section('content')
    <div class="swiper-container">
        <div class="swiper-wrapper">
            @foreach($slider as $post)
            <div class="swiper-slide">
                <a href="/post/{{ $post->slug }}">
                    <div class="slider-post">
                        <img src="{{ $post->slider_image }}" alt="Glovo">
                        <div class="container">
                            <div class="slider-post-title">{{ $post->title }}</div>
                            <div class="slider-post-tags">
                                <a href="/category/{{ $post->category->code }}">{{ $post->category->name }}</a>
                            </div>
                            <a href="/tags/{{ mb_strtolower($post->tags()->first()->name) }}" class="slider-post-hashtag">#{{ $post->tags()->first()->name }}</a>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <div class="swiper-pagination"></div>

        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>

    <div class="grid container-fluid">
        @if($tiles && $tiles->count() === 6)
        <div class="row">
            <div class="col-md-6">
                <a href="/post/{{ $tiles->first()->post->slug }}">
                    <div class="tile tile-medium" style="background-image:url('/{{ $tiles->first()->img }}');">
                        <div class="tile-text" style="background-color:{{ $tiles->first()->post->color }}">
                            <div class="tile-label">{{ $tiles->first()->post->categoryName }}</div>
                            <div class="tile-title">{!! $tiles->first()->post->title !!}</div>
                        </div>
                    </div>
                    <span class="tile-background-mobile" style="background-image:url('/{{ $tiles->first()->img }}');"></span>
                </a>
            </div>
            <div class="col-md-3">
                <a href="/post/{{ $tiles[1]->post->slug }}">
                    <div class="tile tile-small" style="background-image:url('/{{ $tiles[1]->img }}');">
                        <div class="tile-text" style="background-color:{{ $tiles[1]->post->color }}">
                            <div class="tile-label">{{ $tiles[1]->post->categoryName }}</div>
                            <div class="tile-title">{!! $tiles[1]->post->title !!}</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="/post/{{ $tiles[2]->post->slug }}">
                    <div class="tile tile-small" style="background-image:url('/{{ $tiles[2]->img }}');">
                        <div class="tile-text" style="background-color:{{ $tiles[2]->post->color }}">
                            <div class="tile-label">{{ $tiles[2]->post->categoryName }}</div>
                            <div class="tile-title">{!! $tiles[2]->post->title !!}</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 native-rklm mobile">
                <div>Реклама</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="tile tile-large p-0">
                    <a href="/post/{{ $tiles[3]->post->slug }}">
                        <div class="tile-img" style="background-image: url('/media/post/{{ $tiles[3]->post->img }}')"></div>
                    </a>
                    <div class="tile-text">
                        <div class="tile-label">{{ $tiles[3]->post->categoryName }}</div>
                        <div class="tile-title"><a href="/post/{{ $tiles[3]->post->slug }}">{{ $tiles[3]->post->title }}</a></div>
                        <div class="tile-date">{{ $tiles[3]->post->created }} <span class="views-count">{{ $tiles[3]->post->views }}</span></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tile tile-square p-0">
                    <a href="/post/{{ $tiles[4]->post->slug }}">
                        <div class="tile-img" style="background-image: url('/media/post/{{ $tiles[4]->post->img }}')"></div>
                    </a>
                    <div class="tile-text">
                        <div class="tile-label">{{ $tiles[4]->post->categoryName }}</div>
                        <div class="tile-title"><a href="/post/{{ $tiles[4]->post->slug }}">{{ $tiles[4]->post->title }}</a></div>
                        <div class="tile-date">{{ $tiles[4]->post->created }} <span class="views-count">{{ $tiles[4]->post->views }}</span></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tile tile-square p-0">
                    <a href="/post/{{ $tiles[5]->post->slug }}">
                        <div class="tile-img" style="background-image: url('/media/post/{{ $tiles[5]->post->img }}')"></div>
                    </a>
                    <div class="tile-text">
                        <div class="tile-label">{{ $tiles[5]->post->categoryName }}</div>
                        <div class="tile-title"><a href="/post/{{ $tiles[5]->post->slug }}">{{ $tiles[5]->post->title }}</a></div>
                        <div class="tile-date">{{ $tiles[5]->post->created }} <span class="views-count">{{ $tiles[5]->post->views }}</span></div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>

    <div class="container-fluid native-rklm-container hidden-mobile">
        <div class="native-rklm">
            Реклама
        </div>
    </div>

    <div class="container-fluid stylized-container new-posts">
        <h2 class="page-heading-h2">Новое на Comode</h2>
        <div class="posts-row row align-items-top justify-content-center">
            @foreach($newPosts as $post)
            <div class="col-md-4">
                <div class="post">
                    <a href="/post/{{ $post->slug }}">
                        <div class="post-img" style="background-image: url('/media/post/{{ $post->img }}')"></div>
                    </a>
                    <div class="post-details">
                        <div class="post-label">{{ $post->categoryName }}</div>
                        <a href="/post/{{ $post->slug }}"><div class="post-title">{{ $post->title }}</div></a>
                        <div class="post-date">{{ $post->created }} <span class="views-count">{{ $post->views }}</span></div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-md-4">
                <div class="post-rklm">
                    <div>Реклама</div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid stylized-container comode-tv mt-5">
        <h2 class="page-heading-h2">Comode TV</h2>
        <div class="comode-tv-slider-wrapper">
            <div class="comode-tv-slider">
                @foreach($comodeTv as $tv)
                <div class="post">
                    <a href="#" class="js-video-btn" data-video-id="{{ $tv->youtube }}">
                        <div class="post-img" style="background-image:url('/media/post/{{ $tv->img }}')"></div>
                    </a>
                    <div class="post-details">
                        <div class="post-label">{{ $tv->categoryName }}</div>
                        <a href="#" class="js-video-btn" data-video-id="{{ $tv->youtube }}">
                            <div class="post-title">{{ $tv->title }}</div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="container-fluid stylized-container columnists mt-5">
        <h2 class="page-heading-h2">Колумнисты</h2>
        <div class="row mx-0" id="columnists">
            @foreach($columnists as $item)
                <div class="col-md-6 mb-3">
                    <div class="columnist-post">
                        <a href="/post/{{ $item->slug }}">
                            <div class="columnist-post-img" style="background-image:url('/media/post/{{ $item->img }}')"></div>
                        </a>
                        <div class="columnist-post-details">
                            <a href="/post/{{ $item->slug }}"><div class="columnist-post-title">{{ $item->titleHtml }}</div></a>
                            <div class="columnist-post-author">{{ $item->author }}</div>
                            <div class="columnist-post-date">{{ $item->created }} <span class="views-count">{{ $item->views }}</span></div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-md-12">
                <button class="show-more show-more-columnists">Смотреть больше</button>
            </div>
        </div>
    </div>

    <div class="container-fluid stylized-container newsletter-container">
        <div class="newsletter">
            <div class="row">
                <div class="col-md-8">
                    <div class="newsletter-title">Подпишись и будь в курсе всего самого интересного на Comode.kz</div>
                    <form action="{{ route('newsletter') }}" class="form-inline" id="newsletter">
                        @csrf
                        <div class="form-group">
                            <input type="text" placeholder="Ваше имя" name="name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" placeholder="Ваше email" name="email" required>
                        </div>
                        <button class="btn btn-blue">Подписаться</button>
                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('/js/slick.min.js') }}"></script>
    <script src="{{ asset('/js/jquery-modal-video.min.js') }}"></script>
    <script src="{{ asset('/js/swiper-bundle.js') }}"></script>
    <script>
        $(document).ready(function () {
            let mySwiper = new Swiper('.swiper-container', {
                direction: 'horizontal',
                loop: true,
                speed: 700,

                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                },

                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },

            });

            var isMobile = false; //initiate as false
            if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
                isMobile = true;
            }

            if (!isMobile) {
                $(".comode-tv-slider").slick({
                    slidesToShow: 3,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                arrows: false,
                                slidesToShow: 1,
                                rows: 3,
                                slidesPerRow: 1
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                arrows: false,
                                slidesToShow: 1,
                                rows: 3,
                                slidesPerRow: 1
                            }
                        }
                    ]
                });
            }

            $(".comode-tv").find(".post-img").each(function () {
                let height = $(this).width();
                $(this).css({
                    height: height
                });
            });

            $(".js-video-btn").on("click", function (e) {
                e.preventDefault();
                return false;
            });
            $(".js-video-btn").modalVideo();

            $(".show-more-columnists").on("click", function () {
                $.ajax({
                    url: "/get-columnists",
                    data: {
                        count: $(".columnist-post").length
                    },
                    dataType: "json",
                    success(response){
                        if (response['status'] === 'ok') {
                            $(".show-more-columnists").remove();
                            $("#columnists").append(response['html']);
                        }
                    }
                });
            });
        });
    </script>
@endsection
