@extends('layout.app')

@section('content')
    <div class="container py-5 main-content search-page">
        <h2 class="mb-3">Результаты по запросу «{{ request()->get('query') }}»</h2>
        <form action="/search" class="mb-5 form-inline">
            <div class="form-group">
                <input type="text" class="search-input" name="query" required value="{{ request()->get('query') }}">
            </div>
            <button class="show-more mx-1 py-2">Поиск</button>
        </form>
        <div class="row pb-3 mb-3">
            @if($posts && $posts->count() > 0)
                @foreach($posts as $post)
                    <div class="col-md-4 mb-3">
                        <div class="post shadow-sm">
                            <a href="/post/{{ $post->slug }}">
                                <div class="post-img" style="background-image: url('/media/post/{{ $post->img }}')"></div>
                            </a>
                            <div class="post-details">
                                <div class="post-label">{{ $post->categoryName }}</div>
                                <a href="/post/{{ $post->slug }}"><div class="post-title">{{ $post->title }}</div></a>
                                <div class="post-date">{{ $post->created }}
                                    <span class="views-count">{{ $post->views }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-md-12 text">
                    {{ $posts->links() }}
                </div>
            @else
                <div class="col-md-12">
                    <div class="text-center py-5 h5">
                        Ничего не найдено по вашему запросу :(
                    </div>
                </div>
            @endif
        </div>
    </div>
    <hr>
@endsection
