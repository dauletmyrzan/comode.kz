@extends('layout.app')

@section('content')
    <div id="category" class="main-content">
        <h1 class="text-center">
            @if(isset($category))
                {{ $category->name }}
            @else
                #{{ $tag->name }}
            @endif
        </h1>
        <div class="container-fluid stylized-container">
            @if(isset($category))
                {!! $category->breadcrumbs !!}
            @endif
            <div class="row border-bottom pb-5 mb-3">
                @if($posts && $posts->count() > 0)
                    @foreach($posts as $post)
                        <div class="col-md-4 mb-3">
                            <div class="post shadow-sm">
                                <a href="/post/{{ $post->slug }}">
                                    <div class="post-img" style="background-image: url('/media/post/{{ $post->img }}')"></div>
                                </a>
                                <div class="post-details">
                                    <div class="post-label">{{ $post->categoryName }}</div>
                                    <a href="/post/{{ $post->slug }}"><div class="post-title">{{ $post->title }}</div></a>
                                    <div class="post-date">{{ $post->created }}
                                        <span class="views-count">{{ $post->views }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-md-12 text">
                        {{ $posts->links() }}
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="text-center py-5 h5">
                            Нет статей по данной категории :(
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
