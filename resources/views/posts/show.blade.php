@extends('layout.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/jssocials.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/jssocials-theme-flat.css') }}">
    @if($post->isKz)
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
        <style>
            .article-body p, .article-body a, .breadcrumb a {
                font-family: 'Roboto', sans-serif!important;
            }
        </style>
    @endif
@endsection

@section('content')
    <div class="container-fluid px-0 main-content">
        <div class="article-header py-3 pb-4 mb-3">
            <div class="article-bg" style="background-image:url('/storage/{{ $post->backgroundImage }}')"></div>
            <div class="container-fliuid stylized-container mx-auto px-0">
                <div class="row">
                    <div class="col-md-6">
                        <div class="article-categories">
                            <a href="/category/{{ $post->category->code }}">{{ $post->category->name }}</a>
                        </div>
                    </div>
                    <div class="col-md-6 text-md-right">
                        <div class="article-tags">
                            @foreach($post->tags()->take(3)->get() as $tag)
                                <a href="/tags/{{ mb_strtolower($tag->name) }}">#{{ $tag->name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <h1>{{ $post->title }}</h1>
                <div class="article-date">
                    {{ $post->created }}
                    <span class="views-count">{{ $post->views }}</span>
                </div>
                <div class="share-socials-white"></div>
            </div>
        </div>
    </div>
    <div class="container-fluid stylized-container bg-white p-4 pb-5" id="article">
        {!! $post->breadcrumbs !!}
        <div class="row">
            <div class="col-md-9">
                @if ($post->audio)
                <div class="article-audio mb-3">
                    <div class="article-audio-label">Слушать статью</div>
                </div>
                <audio controls>
                    <source src="/storage/{{ $post->audio }}">
                </audio>
                @endif
                <div class="article-body">
                    {!! $post->body !!}
                </div>
                <div class="share-socials-black"></div>
            </div>
            <div class="col-md-3">
                <div class="post-rklm mb-3">
                    <div>Реклама</div>
                </div>
                <div class="similar-posts">
                @foreach($post->similar as $similarPost)
                    <div class="post shadow-sm">
                        <a href="{{ $similarPost->slug }}">
                            <div class="post-img similar-post-img" style="background-image: url('/media/post/{{ $similarPost->img }}')"></div>
                        </a>
                        <div class="post-details">
                            <div class="post-label">{{ $similarPost->categoryName }}</div>
                            <a href="{{ $similarPost->slug }}"><div class="post-title">{{ $similarPost->title }}</div></a>
                            <div class="post-date">{{ $similarPost->created }}
                                <span class="views-count">{{ $similarPost->views }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid stylized-container newsletter my-0 mx-auto mb-5">
        <div class="row">
            <div class="col-md-8">
                <div class="newsletter-title">Подпишись и будь в курсе всего самого интересного на Comode.kz</div>
                <form action="{{ route('newsletter') }}" class="form-inline" id="newsletter">
                    <div class="form-group">
                        <input type="text" placeholder="Ваше имя" name="name" required>
                    </div>
                    <div class="form-group">
                        <input type="email" placeholder="Ваше email" name="email" required>
                    </div>
                    <button class="btn btn-blue">Подписаться</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('/js/jssocials.min.js') }}"></script>
    <script src="{{ asset('/js/slick.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".share-socials-white").jsSocials({
                showLabel: false,
                shares: [{
                    share: "telegram",
                    logo: "/img/social/telegram-white.svg",
                }, {
                    share: "facebook",
                    logo: "/img/social/facebook-white.svg",
                }, {
                    share: "whatsapp",
                    logo: "/img/social/whatsapp-white.svg",
                }, {
                    share: "email",
                    logo: "/img/social/mail-white.svg",
                }, {
                    share: "twitter",
                    logo: "/img/social/twitter-white.svg",
                }]
            });

            $(".share-socials-black").jsSocials({
                showLabel: false,
                shares: [{
                    share: "telegram",
                    logo: "/img/social/telegram.svg",
                }, {
                    share: "facebook",
                    logo: "/img/social/facebook.svg",
                }, {
                    share: "whatsapp",
                    logo: "/img/social/whatsapp.svg",
                }, {
                    share: "email",
                    logo: "/img/social/mail.svg",
                }, {
                    share: "twitter",
                    logo: "/img/social/twitter.svg",
                }]
            });

            function processGalleries() {
                let ids = [];

                $('.gallery-placeholder').each(function () {
                    let id = parseInt($(this).attr('class').replace(/[^0-9]/gi,''));
                    ids.push(id);
                });

                $.ajax({
                    url: "/get-galleries",
                    data: {
                        ids: ids,
                    },
                    success (response) {
                        if (response['status'] === 'ok') {
                            let html = $(response['data']);
                            for (let i = 0; i < html.length; i++) {
                                let slider = $(html[i]);
                                $(".article-body").find('.__gallery__' + slider.data('id') + '__').html(slider);
                            }

                            $('.article-body').find('.slick-slider').slick({
                                slidesToShow: 1,
                                arrows: true,
                                dots: true,
                            });
                        }
                    }
                });
            }

            processGalleries();

            $('.article-body').find('img').each(function (i) {
                let k = $(this).outerWidth() / $(this).outerHeight();

                if ($(window).outerWidth() < 768 || k >= 1.7) {
                    $(this).css({
                        'width': '100%',
                        'height': 'initial'
                    });
                    const $elem = $(this);
                    $elem[0].style.setProperty('height', 'initial', 'important');
                }
            });
        });
    </script>
@endsection
