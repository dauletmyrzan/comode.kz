<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->boolean('draft');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('url')->nullable();
            $table->string('main_image')->nullable();
            $table->longText('content')->nullable();
            $table->integer('view_count')->default(0);
            $table->integer('comment_count')->default(0);
            $table->dateTime('date_published');
            $table->string('list_image')->nullable();
            $table->string('olia_image')->nullable();
            $table->text('main_page_description')->nullable();
            $table->string('slider_image')->nullable();
            $table->boolean('show_slider_details')->default(1);
            $table->boolean('show_in_slider')->default(0);
            $table->boolean('comode_girl')->default(0);
            $table->boolean('approved')->default(0);
            $table->dateTime('date_approved')->nullable();
            $table->boolean('updated_by_editor')->default(0);
            $table->smallInteger('status')->nullable();
            $table->boolean('type')->default(1);
            $table->boolean('expert')->default(0);
            $table->string('main_expert_image')->nullable();
            $table->string('list_expert_image')->nullable();
            $table->boolean('show_in_expert_slider')->default(0);
            $table->boolean('show_opinion_mark')->default(0);
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_image')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('listbig_image')->nullable();
            $table->boolean('show_blog_post')->default(0);
            $table->string('nestle_anons')->nullable();
            $table->string('nestle_add')->nullable();
            $table->string('nestle_add2')->nullable();
            $table->string('nestle_add3')->nullable();
            $table->string('nestle_girl')->nullable();
            $table->string('nestle_main')->nullable();
            $table->boolean('informer_main')->nullable();

            $table->timestamps();

            $table->unsignedBigInteger('post_id')->nullable();
            $table->boolean('is_tv')->default(false);
            $table->string('link')->nullable();
            $table->boolean('is_columnist')->default(false);
            $table->string('audio')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
