<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableUsersAddColumns2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('login')->nullable();
            $table->string('pass')->nullable();
            $table->string('activation_key')->nullable();
            $table->string('activated')->nullable();
            $table->boolean('blocked')->nullable();
            $table->boolean('deleted')->nullable();
            $table->dateTime('date_created')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->dateTime('date_deleted')->nullable();
            $table->boolean('notify_comment_own_informer')->default(1);
            $table->boolean('notify_comment_fav_informer')->default(1);
            $table->boolean('notify_comment_reply_informer')->default(1);
            $table->boolean('expert')->default(0);
            $table->string('expert_avatar')->nullable();
            $table->text('expert_activity')->nullable();
            $table->text('expert_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('login');
            $table->dropColumn('pass');
            $table->dropColumn('activation_key');
            $table->dropColumn('activated');
            $table->dropColumn('blocked');
            $table->dropColumn('deleted');
            $table->dropColumn('date_created');
            $table->dropColumn('date_updated');
            $table->dropColumn('date_deleted');
            $table->dropColumn('notify_comment_own_informer');
            $table->dropColumn('notify_comment_fav_informer');
            $table->dropColumn('notify_comment_reply_informer');
            $table->dropColumn('expert');
            $table->dropColumn('expert_avatar');
            $table->dropColumn('expert_activity');
            $table->dropColumn('expert_description');
        });
    }
}
