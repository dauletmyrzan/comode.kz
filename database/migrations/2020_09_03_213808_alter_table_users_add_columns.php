<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableUsersAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('about')->nullable();
            $table->string('avatar')->nullable();
            $table->string('gender')->nullable();
            $table->string('birthdate')->nullable();
            $table->string('account_vk')->nullable();
            $table->string('account_fb')->nullable();
            $table->string('account_mailru')->nullable();
            $table->string('account_twitter')->nullable();
            $table->string('photo')->nullable();
            $table->string('city')->nullable();
            $table->string('notify_email')->nullable();
            $table->string('role')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('about');
            $table->dropColumn('avatar');
            $table->dropColumn('gender');
            $table->dropColumn('birthdate');
            $table->dropColumn('account_vk');
            $table->dropColumn('account_fb');
            $table->dropColumn('account_mailru');
            $table->dropColumn('account_twitter');
            $table->dropColumn('photo');
            $table->dropColumn('city');
            $table->dropColumn('notify_email');
            $table->dropColumn('role');
        });
    }
}
