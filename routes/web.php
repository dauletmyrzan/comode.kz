<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/post/{slug}', 'PostController@show');

Route::get('/get-columnists', 'PostController@getColumnists');

Route::get('/category/{code}', 'PostController@index');

Route::get('/tags/{name}', 'TagController@index');

Route::get('/search', 'PostController@search');

Route::post('/newsletter', 'HomeController@newsletter')->name('newsletter');

Route::get('/phpinfo', function(){echo phpinfo();});

Route::get('/rules', function () {
    return view('rules');
});

Route::get('/page/rules', function () {
    return redirect('/rules', 302);
});

Route::get('/contacts', function () {
    return view('contacts');
});

Route::get('/page/contacts', function () {
    return redirect('/contacts', 302);
});

Auth::routes();

Route::get('/home', function () {
    return redirect('/', 302);
})->name('home');

Route::get('/magazine/subject/{code}', function ($code) {
    if ($code === 'novosti') {
        return redirect('/', 302);
    }

    return redirect('/category/' . $code, 302);
});

Route::get('/magazine', function () {
    return redirect('/', 302);
});

Route::get('/magazine/category/{code}', function ($code) {
    if ($code === 'zdorovye-privychki') {
        $code = 'krasota-i-zdorove';
    }

    return redirect('/category/' . $code, 302);
});

Route::get('/get-galleries', 'GalleryController@get');
